import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { NumberDeviderPipe } from '../pipes/number-devider.pipe';
import { NumberRoundPipe } from '../pipes/number-round.pipe';
import { StringObjectsPipe } from '../pipes/string-objects.pipe';
import { RouterModule } from '@angular/router';

const DECLARATIONS = [
  NumberDeviderPipe,
  NumberRoundPipe,
  StringObjectsPipe
];

const IMPORTS = [
  CommonModule,
  ReactiveFormsModule,
  RouterModule
];

@NgModule({
  imports: [
    ...IMPORTS
  ],
  declarations: [
    ...DECLARATIONS
  ],
  exports: [
    ...IMPORTS,
    ...DECLARATIONS
  ]
})
export class SharedModule { }
