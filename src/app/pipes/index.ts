import { NgModule } from '@angular/core';
import { NumberDeviderPipe } from './number-devider.pipe';


export const PIPES = [
  NumberDeviderPipe
];

@NgModule({
  declarations: PIPES,
  exports: PIPES,
})
export class RcPipesModule {}
