import { Pipe, PipeTransform } from '@angular/core';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({name: 'toObjects'})
export class StringObjectsPipe implements PipeTransform {

  transform(value: string): string {
    return this.recognizer(value);
  }

  recognizer(value: string) {
    console.log('value:', value);
    const lastChar = value.slice(-1);
    switch (lastChar) {
      case '0':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
        return `${value} объектов`;
      case '1':
        return `${value} объект`;
      case '2':
      case '3':
      case '4':
        return `${value} объекта`;
    }
  }
}
