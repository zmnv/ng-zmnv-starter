import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { HomeComponent } from './containers/home-page.component';

// import { MaterialModule } from '@app/shared/material.module';
import { SharedModule } from '@app/shared/shared.module';
import { CoreComponentsModule } from './components';

export const MODULES = [
  // MaterialModule,
  SharedModule,
  CoreComponentsModule
];

export const COMPONENTS = [
  AppComponent,
  HomeComponent,
  // NotFoundPageComponent
];

@NgModule({
  imports: [...MODULES],
  declarations: [...COMPONENTS],
  exports: [...MODULES, ...COMPONENTS]
})
export class CoreModule {}
