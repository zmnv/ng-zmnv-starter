import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';

export const COMPONENTS = [
];

@NgModule({
  imports: [
    SharedModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class CoreComponentsModule {}
