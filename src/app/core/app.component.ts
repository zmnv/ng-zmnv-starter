import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: /*html*/`
    <div>
      Application
      <router-outlet></router-outlet>
    </div>
  `
})
export class AppComponent {

}
