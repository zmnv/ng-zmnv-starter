import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  template: /*html*/ `
    <div>
      Home page
    </div>
  `,
})
export class HomeComponent {

}
