import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {TransferHttpCacheModule} from '@nguniversal/common';

import { AppRoutingModule } from '@app/app-routing.module';

import { SharedModule } from '@app/shared/shared.module';
import { AppComponent } from '@app/core/app.component';
import { CoreModule } from './core/core.module';

@NgModule({
  declarations: [
  ],
  imports: [
    CoreModule,
    SharedModule,
    BrowserModule.withServerTransition({appId: 'my-app'}),
    AppRoutingModule,
    TransferHttpCacheModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
